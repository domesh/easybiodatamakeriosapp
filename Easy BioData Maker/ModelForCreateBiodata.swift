//
//  ModelForCreateBiodata.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 06/06/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import Foundation


struct PersonalInfo {
    var fullName: String
    var dob: String
    var tob: String
    var pob: String
    var height: String
    var complexity: String

}

struct familyInfo {
    var fatherName: String
    var motherName: String
    var brotherName: String
    var sisterName: String
    var fatherOccupation: String
    var motherOccupation: String
}

struct culture {
    var religion: String
    var caste: String
    var gotra: String
    var zodiac: String
    var languages: String
}


