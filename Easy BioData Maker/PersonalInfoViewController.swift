//
//  PersonalInfoViewController.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 05/06/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit


protocol PersonalInfoData {
    func sendPersonalIInfoData(dict: PersonalInfo, section: Int)
}
class PersonalInfoViewController: UIViewController {

    @IBOutlet weak var txtCompexity: UITextField!
    @IBOutlet weak var txtHeight: UITextField!
    @IBOutlet weak var txtPOB: UITextField!
    @IBOutlet weak var txtTOB: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtFullNAme: UITextField!
    var delegate: PersonalInfoData?
  
    override func viewDidLoad() {
        super.viewDidLoad()

    
    }

    @IBAction func btnSubmitClicked(_ sender: Any) {
       
       
        let fullName = txtFullNAme.text
        let dob = txtDOB.text
        let pob = txtPOB.text
        let tob = txtTOB.text
        let complexity = txtCompexity.text
        let height = txtHeight.text
        
        let personalInfo = PersonalInfo(fullName: fullName!, dob: dob!, tob: tob!, pob: pob!, height: height!, complexity: complexity!)
        delegate?.sendPersonalIInfoData(dict: personalInfo, section: 1)
        btnBackClicked(self)
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
}
