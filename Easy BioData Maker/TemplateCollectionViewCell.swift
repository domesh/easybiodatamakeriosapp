//
//  TemplateCollectionViewCell.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 30/05/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit

class TemplateCollectionViewCell: UICollectionViewCell {
     var actionBlock: (() -> Void)? = nil
    @IBOutlet weak var btnCreateBioData: UIButton!
    @IBOutlet weak var imgTemplate: UIImageView!
    
    @IBAction func btnCreateBioDataClicked(_ sender: Any) {
        actionBlock?()
    }
    
}
