//
//  ChooseTemplateViewController.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 30/05/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit

class ChooseTemplateViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
   
    

   
    @IBOutlet weak var collectionView: UICollectionView!
    var arrayTemplate = [Dictionary<String, String>]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callWebServices()
       }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func callWebServices(){
//        WServices.shared.getResponseFromURL(url: "c", params: ["":""]) { (result) in
//            NSLog("response \(result)")
//            self.arrayTemplate = result["template_list"] as! [Dictionary<String, String>]
//        }
        
        self.arrayTemplate = [["id":"0", "image_url":"https://picsum.photos/200/300"],["id":"1", "image_url":"https://picsum.photos/200"]]
        collectionView.reloadData()
    }

    
    func downloadImage(from url: URL,forCell cell: TemplateCollectionViewCell){
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                cell.imgTemplate.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayTemplate.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "templateCell", for: indexPath) as! TemplateCollectionViewCell
        let templateDict = self.arrayTemplate[indexPath.row]
        let imageURL = templateDict["image_url"]
        self.downloadImage(from: URL(string: imageURL ?? "")!, forCell: cell)
        cell.actionBlock = {
            let viewController = self.storyboard!.instantiateViewController(withIdentifier: "bioDataInfo") as! BioDataInfoViewController
            if let navController = self.navigationController {
                navController.pushViewController(viewController, animated: true)
            }
        }
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "bioDataInfo") as! BioDataInfoViewController
        self.navigationController!.pushViewController(viewController, animated: true)
    }
//    @IBAction func btnBackClicked(_ sender: Any) {
//        if let navController = self.navigationController {
//            navController.popViewController(animated: true)
//        }
//    }
}
