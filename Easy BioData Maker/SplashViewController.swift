//
//  SplashViewController.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 28/05/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit
import Alamofire

class SplashViewController: UIViewController {

    var dictWelcome = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callWebServices(url: "")
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination as! WalkThroughViewController
        viewController.dictWalkThrough = dictWelcome
    }
    func callWebServices(url:String) {
        // TODO: Splash Screen URL
        // url :/bio_data_maker/splash_screen/
//        WServices.shared.getResponseFromURL(url: url, params: ["":""]) { (result) in
//            NSLog("response \(result)")
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            if(UserDefaults.standard.bool(forKey: "isAppAlreadyLaunched")){
//                let templateViewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chooseTemplate") as! ChooseTemplateViewController
//                let navController = UINavigationController(rootViewController: templateViewController)
//                guard let window = UIApplication.shared.keyWindow else { return }
//                window.addSubview(navController.view)
//                window.makeKeyAndVisible()
                let viewController = self.storyboard!.instantiateViewController(withIdentifier: "chooseTemplate") as! ChooseTemplateViewController
                self.navigationController!.pushViewController(viewController, animated: true)
            }else{
                UserDefaults.standard.set(true, forKey: "isAppAlreadyLaunched")
                self.dictWelcome = ["success":true,"message" : "Success","welcome_page": [["id" : 1,"image_url": "https://picsum.photos/200","message": "Something to write"],["id" : 2,"image_url": "https://picsum.photos/200","message": "Something to write"],["id" : 2,"image_url": "https://picsum.photos/200","message": "Something to write"]]]
                let viewController = self.storyboard!.instantiateViewController(withIdentifier: "walkThrough") as! WalkThroughViewController
                viewController.dictWalkThrough = self.dictWelcome
                self.navigationController!.pushViewController(viewController, animated: true)
                //self.performSegue(withIdentifier: "toWalkThrough", sender: nil)
            }
            
        })
    }
    
    
    
}
