//
//  WalkThroughViewController.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 29/05/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit


class WalkThroughViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    var dictWalkThrough = [String:Any]()
    var pageTitles = [String]()
    var pageImagesURLs = [String]()
    var pageContainer: UIPageViewController!
    var currentIndex: Int?
    private var pendingIndex: Int?
    @IBOutlet weak var pageControls: UIPageControl!
    lazy var pages: [UIViewController] = {
        return [
            self.getViewController(withIndex: 0),
            self.getViewController(withIndex: 1),
            self.getViewController(withIndex: 2)
        ]
    }()
    
    func getViewController(withIndex index: Int) -> UIViewController
    {
        let childViewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "childWalkThrough") as! ChildWalkThroughViewController
        if pageTitles.isEmpty == false {
            childViewController.StringLabel = pageTitles[index]
            childViewController.imageURL = pageImagesURLs[index]
            if index == pageTitles.count-1{
                childViewController.isLastViewController = true
            }
        }
        
        return childViewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenInitailization()
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.delegate = self
        pageContainer.dataSource = self
        pageContainer.setViewControllers([pages[0]], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        view.addSubview(pageContainer.view)
        
        view.bringSubviewToFront(pageControls)
        pageControls.numberOfPages = pages.count
        pageControls.currentPage = 0
        pageControls.pageIndicatorTintColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
        pageControls.currentPageIndicatorTintColor = #colorLiteral(red: 0.9607843137, green: 0.4666666667, blue: 0.1215686275, alpha: 1)
    }
    
    func screenInitailization(){
        let arrayWalkThroughs: NSArray = dictWalkThrough["welcome_page"] as! NSArray
        for index in 0...arrayWalkThroughs.count-1 {
            let dictTemp = arrayWalkThroughs.object(at: index) as! Dictionary<String, Any>
            let titleString = dictTemp["message"] as! String
            let url = dictTemp ["image_url"] as! String
            pageTitles.append(titleString)
            pageImagesURLs.append(url)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)
        if currentIndex == 0 {
            return nil
        }
        let previousIndex = abs((currentIndex! - 1) % pages.count)
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        if currentIndex == pages.count-1 {
            return nil
        }
        let nextIndex = abs((currentIndex + 1) % pages.count)
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingIndex = pages.index(of: pendingViewControllers.first!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            currentIndex = pendingIndex
            if let index = currentIndex {
                pageControls.currentPage = index
            }
        }
    }
}
