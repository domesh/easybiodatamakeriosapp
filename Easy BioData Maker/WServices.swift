//
//  WServices.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 28/05/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import Foundation
import Alamofire

class WServices{
    static let shared = WServices()
    init() {}
    let baseURL = "https://api.github.com/search/repositories?q="
    func getResponseFromURL(url: String, params: Dictionary<String, Any>, completionHandler: @escaping(_ result: [String: Any]) -> Void)  {
        let completeURL = getCompleteURL(urlString: url)
        Alamofire.request(completeURL)
            .responseJSON { response in
                
                let error = response.result.error
                guard error == nil else {
                    print("error calling GET on /todos/1")
                    print(response.result.error!)
                    return
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    if let error = response.result.error {
                        print("Error: \(error)")
                    }
                    return
                }
                
                
                guard (json["success"] as?  String) != "True" else {
                    print("Response \(String(describing: json["message"]))")
                    return
                }
                
                do {
                    completionHandler(json)
                }
        }
    }
    
    private func getCompleteURL(urlString:String) -> String {
        let completeURL :String
        if urlString.contains("http")||urlString.contains("https") {
            completeURL = urlString
        }else{
            completeURL = baseURL + urlString
        }
        return completeURL
    }
    
//    func getImageFromURL(imageUrl:String)->UIImage{
//        if let url = NSURL(string: imageUrl) {
//            let request = NSURLRequest(url: url as URL)
//            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
//                (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
//                self.image = UIImage(data: data)
//            }
//        }
//    }
//
   
}
