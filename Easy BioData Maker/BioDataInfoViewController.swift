//
//  BioDataInfoViewController.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 02/06/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit


enum ProfileViewModelItemType {
    case image
    case personalInfo
    case familyInfo
    case culture
    case paternal
    case maternal
    case contact
    case workAndEducation
}

enum InfoTypeModel{
    case personalInfo()
}

protocol ProfileViewModelItem {
    var type: ProfileViewModelItemType { get }
    var sectionTitle: String { get }
    var isCollapsible: Bool { get }
    var isCollapsed: Bool { get set }
    var rowCount: Int { get }
    var isDataAvailable: Bool { get set }
}

extension ProfileViewModelItem {
    var rowCount: Int {
        return 1
    }
    
    var isCollapsible: Bool {
        return true
    }
}


class BioDataInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, HeaderViewDelegate, PersonalInfoData {
    
    
    @IBOutlet weak var lblBioDataName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var items = [ProfileViewModelItem]()
    var reloadSections: ((_ section: Int) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presentAlert()
        initNIBs()
        fillViewWithDetails()
        tableView.sectionHeaderHeight = 44
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func fillViewWithDetails(){
        let imageName = ""
        let imageInfo = ViewModelImagesItem(imageName: imageName)
        items.append(imageInfo)
        
        let name = "Pooja Agrawal"
        let Dob = "25/03/1993"
        let personalInfo = ViewModelPersonalItem(name: name, dob: Dob)
        items.append(personalInfo)
        
        let worknEducation = ViewModelWorkEducationItem(work: "Self Employed", education: "B.Tech")
        items.append(worknEducation)
        
        let culture = ViewModelCulturalInfoItem(religion: "Hindu", caste: "Marwadi", gotra: "Garg", zodiac: "Aries", languages: "Hindi")
        items.append(culture)
        
        let familyInfo = ViewModelFamilyInfoItem(fatherName: "Deepak", motherName: "Suman", brotherName: "Sudeep", sisterName: "Surbhi", fatherOccupation: "business", motherOccupation: "family")
        items.append(familyInfo)
        
        let paternal = ViewModelPaternalInfoItem(fatherName: "xyz")
        items.append(paternal)
        
        let  maternal = ViewModelMaternalInfoItem(fatherName: "xyz")
        items.append(maternal)
        
        let contact = ViewModelContactInfoItem(fatherName: "xyz")
        items.append(contact)
        
    }
    
    func initNIBs(){
        tableView.register(TableSectionHeader.nib, forHeaderFooterViewReuseIdentifier: TableSectionHeader.identifier)
        tableView.register(ImageCell.nib, forCellReuseIdentifier: ImageCell.identifier)
        tableView.register(PersonalInfoCell.nib, forCellReuseIdentifier: PersonalInfoCell.identifier)
        tableView.register(FamilyInfoCell.nib, forCellReuseIdentifier: FamilyInfoCell.identifier)
        tableView.register(CulturalInfoCell.nib, forCellReuseIdentifier: CulturalInfoCell.identifier)
        tableView.register(WorkAndEducationCell.nib, forCellReuseIdentifier: WorkAndEducationCell.identifier)
        tableView.register(PaternalInfoCell.nib, forCellReuseIdentifier: PaternalInfoCell.identifier)
        tableView.register(MaternalInfoCell.nib, forCellReuseIdentifier: MaternalInfoCell.identifier)
        tableView.register(ContactDetailsCell.nib, forCellReuseIdentifier: ContactDetailsCell.identifier)
    }
    
    func presentAlert(){
        let alert = UIAlertController(title: "Create biodate", message: "Enter title of document", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Title name"
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert!.textFields![0] as UITextField
            self.lblBioDataName.text = textField.text!
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alertAction) in
        }))
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func toggleSection(header: TableSectionHeader, section: Int) {
        var item = items[section]
        
        if !item.isDataAvailable {
            openVC(itemType: item.type)
        }else{
            
            if item.isCollapsible {
                
                let collapsed = !item.isCollapsed
                item.isCollapsed = collapsed
                
                tableView.beginUpdates()
                tableView.reloadSections([section], with: .automatic)
                tableView.endUpdates()
            }
        }
    }
    
    func openVC(itemType: ProfileViewModelItemType){
        var viewController = UIViewController()
                switch itemType {
                case .image:
                     viewController = self.storyboard!.instantiateViewController(withIdentifier: "bioDataInfo") as! BioDataInfoViewController
                    self.navigationController!.pushViewController(viewController, animated: true)
                case .personalInfo:
                     viewController = self.storyboard!.instantiateViewController(withIdentifier: "personalInfo") as! PersonalInfoViewController
                    self.navigationController!.pushViewController(viewController, animated: true)
                case .culture:
                     viewController = self.storyboard!.instantiateViewController(withIdentifier: "bioDataInfo") as! BioDataInfoViewController
                    self.navigationController!.pushViewController(viewController, animated: true)
                case .familyInfo:
                     viewController = self.storyboard!.instantiateViewController(withIdentifier: "bioDataInfo") as! BioDataInfoViewController
                    self.navigationController!.pushViewController(viewController, animated: true)
                case .maternal:
                     viewController = self.storyboard!.instantiateViewController(withIdentifier: "bioDataInfo") as! BioDataInfoViewController
                    self.navigationController!.pushViewController(viewController, animated: true)
                case .paternal:
                     viewController = self.storyboard!.instantiateViewController(withIdentifier: "bioDataInfo") as! BioDataInfoViewController
                    self.navigationController!.pushViewController(viewController, animated: true)
                case .workAndEducation:
                     viewController = self.storyboard!.instantiateViewController(withIdentifier: "bioDataInfo") as! BioDataInfoViewController
                    self.navigationController!.pushViewController(viewController, animated: true)
                case .contact:
                     viewController = self.storyboard!.instantiateViewController(withIdentifier: "bioDataInfo") as! BioDataInfoViewController
                    self.navigationController!.pushViewController(viewController, animated: true)
                }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let item = items[section]
        guard item.isCollapsible else {
            return item.rowCount
        }
        
        if item.isCollapsed {
            return 0
        } else {
            return item.rowCount
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: TableSectionHeader.identifier)
        let header = cell as! TableSectionHeader
        header.lblTitle.text = title
        let item = items[section]
        header.item = item
        header.section = section
        header.delegate=self
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        switch item.type {
        case .image:
            if let cell = tableView.dequeueReusableCell(withIdentifier: ImageCell.identifier, for: indexPath) as? ImageCell {
                cell.item = item
                return cell
            }
        case .personalInfo:
            if let cell = tableView.dequeueReusableCell(withIdentifier: PersonalInfoCell.identifier, for: indexPath) as? PersonalInfoCell {
                cell.item = item
                return cell
            }
        case .familyInfo:
            if let cell = tableView.dequeueReusableCell(withIdentifier: FamilyInfoCell.identifier, for: indexPath) as? FamilyInfoCell {
                cell.item = item
                return cell
            }
        case .culture:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CulturalInfoCell.identifier, for: indexPath) as? CulturalInfoCell {
                cell.item = item
                return cell
            }
        case .workAndEducation:
            if let cell = tableView.dequeueReusableCell(withIdentifier: WorkAndEducationCell.identifier, for: indexPath) as? WorkAndEducationCell {
                cell.item = item
                return cell
        }
        case .paternal:
            if let cell = tableView.dequeueReusableCell(withIdentifier: PaternalInfoCell.identifier, for: indexPath) as? PaternalInfoCell {
                cell.item = item
                return cell
            }
        case .maternal:
            if let cell = tableView.dequeueReusableCell(withIdentifier: MaternalInfoCell.identifier, for: indexPath) as? MaternalInfoCell {
                cell.item = item
                return cell
            }
        case .contact:
            if let cell = tableView.dequeueReusableCell(withIdentifier: ContactDetailsCell.identifier, for: indexPath) as? ContactDetailsCell {
                cell.item = item
                return cell
            }
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 || section == 4 || section == 6{
            return 28
        }else{
            return 0
        }
    }
    
    
    @IBAction func btnPreviewClicked(_ sender: Any) {
    }
    
    @IBAction func btnSavenNextClicked(_ sender: Any) {
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func sendPersonalIInfoData(dict: PersonalInfo, section: Int) {
        var item = items[section]
        item.isDataAvailable = true
    }
}

class ViewModelImagesItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .image
    }
    
    var isCollapsible: Bool {
        return true
    }
    
    var isCollapsed = true
    
    var isDataAvailable = true
    
    var imageURL: String
    var sectionTitle: String {
        return "Images"
    }
    init(imageName: String) {
        self.imageURL = imageName
    }
}


class ViewModelPersonalItem: ProfileViewModelItem {
    var isDataAvailable = false
    var type: ProfileViewModelItemType {
        return .personalInfo
    }
    
    var isCollapsible: Bool {
        return true
    }
    
    var isCollapsed = true
    
    var name: String
    var Dob: String
    var sectionTitle: String {
        return "Personal Info"
    }
    init(name: String, dob: String) {
        self.name = name
        self.Dob = dob
    }
}


class ViewModelWorkEducationItem: ProfileViewModelItem {
    var isDataAvailable = true
    var type: ProfileViewModelItemType {
        return .image
    }
    
    var isCollapsible: Bool {
        return true
    }
    
    var isCollapsed = true
    
    var work: String
    var education: String
    
    var sectionTitle: String {
        return "Work and Education"
    }
    init(work: String, education: String) {
        self.work = work
        self.education = education
    }
}


class ViewModelCulturalInfoItem: ProfileViewModelItem {
    var isDataAvailable = true
    
    var type: ProfileViewModelItemType {
        return .culture
    }
    
    var isCollapsible: Bool {
        return true
    }
    
    var isCollapsed = true
    
    var religion: String
    var caste: String
    var gotra: String
    var zodiac: String
    var languages: String
    
    var sectionTitle: String {
        return "Cultural Information"
    }
    init(religion: String, caste: String, gotra: String, zodiac: String, languages: String) {
        self.religion = religion
        self.caste = caste
        self.gotra = gotra
        self.zodiac = zodiac
        self.languages = languages
    }
}

class ViewModelFamilyInfoItem: ProfileViewModelItem {
    var isDataAvailable = true
    var type: ProfileViewModelItemType {
        return .familyInfo
    }
    
    var isCollapsible: Bool {
        return true
    }
    
    var isCollapsed = true
    
    var fatherName: String
    var motherName: String
    var brotherName: String
    var sisterName: String
    var fatherOccupation: String
    var motherOccupation: String
    
    
    var sectionTitle: String {
        return "Family Information"
    }
    init(fatherName: String, motherName: String, brotherName: String, sisterName: String, fatherOccupation: String, motherOccupation: String) {
        self.fatherName = fatherName
        self.motherName = motherName
        self.brotherName = brotherName
        self.sisterName = sisterName
        self.fatherOccupation = fatherOccupation
        self.motherOccupation = motherOccupation
    }
}


class ViewModelPaternalInfoItem: ProfileViewModelItem {
    var isDataAvailable = true
    
    var type: ProfileViewModelItemType {
        return .paternal
    }
    
    var isCollapsible: Bool {
        return true
    }
    
    var isCollapsed = true
    
    var fatherName: String
    
    
    var sectionTitle: String {
        return "Paternal Information"
    }
    init(fatherName: String) {
        self.fatherName = fatherName
    }
}

class ViewModelMaternalInfoItem: ProfileViewModelItem {
    var isDataAvailable = true
    var type: ProfileViewModelItemType {
        return .maternal
    }
    
    var isCollapsible: Bool {
        return true
    }
    
    var isCollapsed = true
    
    var fatherName: String
    
    
    var sectionTitle: String {
        return "Maternal Information"
    }
    init(fatherName: String) {
        self.fatherName = fatherName
    }
}


class ViewModelContactInfoItem: ProfileViewModelItem {
    var isDataAvailable = true
    var type: ProfileViewModelItemType {
        return .contact
    }
    
    var isCollapsible: Bool {
        return true
    }
    
    var isCollapsed = true
    
    var fatherName: String
    
    
    var sectionTitle: String {
        return "Contact Details"
    }
    init(fatherName: String) {
        self.fatherName = fatherName
    }
}



