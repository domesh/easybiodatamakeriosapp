//
//  ChildWalkThroughViewController.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 29/05/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit
import UIGradient

class ChildWalkThroughViewController: UIViewController {

    @IBOutlet weak var btnGetStarted: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblControllerNumber: UILabel!
    var StringLabel=""
    var isLastViewController=false
    var imageForView = UIImage()
    var imageURL = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let gradient =  GradientLayer(direction: GradientDirection.topToBottom, colors: [#colorLiteral(red: 0.9882352941, green: 0.8705882353, blue: 0.7725490196, alpha: 1),#colorLiteral(red: 0.9960784314, green: 0.9882352941, blue: 0.9803921569, alpha: 1)])
        view.addGradient(gradient)
        lblControllerNumber.text = StringLabel
        //imgView.image = imageForView
        self.downloadImage(from: URL(string: imageURL)!)
        view.bringSubviewToFront(imgView)
        view.bringSubviewToFront(lblControllerNumber)
        if isLastViewController {
            let gradientBtn =  GradientLayer(direction: GradientDirection.leftToRight, colors: [#colorLiteral(red: 0.9607843137, green: 0.4666666667, blue: 0.1215686275, alpha: 1),#colorLiteral(red: 0.9882352941, green: 0.7137254902, blue: 0.262745098, alpha: 1)])
            gradientBtn.cornerRadius = 5
            gradientBtn.frame = btnGetStarted.frame
            gradientBtn.frame.origin.x = btnGetStarted.frame.origin.x
            btnGetStarted.addGradient(gradientBtn, cornerRadius: 5)
            view.bringSubviewToFront(btnGetStarted)
        }
    }
    
    func downloadImage(from url: URL){
       getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.imgView.image = UIImage(data: data)!
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

    @IBAction func btnGetStartedClicked(_ sender: Any) {
        let templateViewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chooseTemplate") as! ChooseTemplateViewController
        let navController = UINavigationController(rootViewController: templateViewController)
        guard let window = UIApplication.shared.keyWindow else { return }
        window.addSubview(navController.view)
        window.makeKeyAndVisible()
    }
    
}
