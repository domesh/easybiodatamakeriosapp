//
//  ImageCell.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 03/06/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {

    @IBOutlet weak var img1: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    var item: ProfileViewModelItem? {
        didSet {
            guard let item = item as? ViewModelImagesItem else {
                return
            }
        
            img1.image = UIImage(named: item.imageURL)
        }
    }

}
