//
//  FamilyInfoCell.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 05/06/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit

class FamilyInfoCell: UITableViewCell {

  
    @IBOutlet weak var lblMothersOccupation: UILabel!
    @IBOutlet weak var lblBrothersName: UILabel!
    @IBOutlet weak var lblSistersName: UILabel!
    @IBOutlet weak var lblMothersName: UILabel!
    @IBOutlet weak var lblFathersOccupation: UILabel!
    @IBOutlet weak var lblFathersName: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    var item: ProfileViewModelItem? {
        didSet {
            guard let item = item as? ViewModelFamilyInfoItem else {
                return
            }
            lblFathersName?.text = item.fatherName
            lblFathersOccupation?.text = item.fatherOccupation
            lblSistersName?.text = item.sisterName
            lblMothersName?.text = item.motherName
            lblMothersOccupation?.text = item.motherOccupation
            lblBrothersName?.text = item.brotherName
        }
    }
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
