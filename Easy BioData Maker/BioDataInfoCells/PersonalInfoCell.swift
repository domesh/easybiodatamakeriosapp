//
//  PersonalInfoCell.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 04/06/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit

class PersonalInfoCell: UITableViewCell {

    @IBOutlet weak var lblPartnerPrefernce: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblTimeOfBirth: UILabel!
    @IBOutlet weak var lblBirthPlace: UILabel!
    @IBOutlet weak var lblPlaceofBirth: UILabel!
    @IBOutlet weak var lblHobbies: UILabel!
    @IBOutlet weak var lblPersonality: UILabel!
    @IBOutlet weak var lbllDietPreference: UILabel!
    
    
    var item: ProfileViewModelItem? {
        didSet {
            guard let item = item as? ViewModelPersonalItem else {
                return
            }
            
            lblName?.text = item.name
            lblDOB?.text = item.Dob
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
