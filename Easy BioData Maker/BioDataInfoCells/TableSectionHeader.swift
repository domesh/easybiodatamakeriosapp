//
//  TableSectionHeader.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 03/06/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit

protocol HeaderViewDelegate: class {
    func toggleSection(header: TableSectionHeader, section: Int)
}


class TableSectionHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var lblTitle: UILabel!
    
    weak var delegate: HeaderViewDelegate?
    var section: Int = 0
    
    var item: ProfileViewModelItem? {
        didSet {
            guard let item = item else {
                return
            }
            lblTitle?.text = item.sectionTitle
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapHeader)))
    }
    
    @objc private func didTapHeader() {
        delegate?.toggleSection(header: self, section: section)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }

}
