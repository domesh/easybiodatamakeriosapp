//
//  MaternalInfoCell.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 05/06/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit

class MaternalInfoCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    var item: ProfileViewModelItem? {
        didSet {
            guard let item = item as? ViewModelMaternalInfoItem else {
                return
            }
            
        }
    }
    
}
