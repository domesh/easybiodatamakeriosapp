//
//  CulturalInfoCell.swift
//  Easy BioData Maker
//
//  Created by Surbhi Bagadia on 05/06/19.
//  Copyright © 2019 CodeNicely. All rights reserved.
//

import UIKit

class CulturalInfoCell: UITableViewCell {

    @IBOutlet weak var lblLanguages: UILabel!
    @IBOutlet weak var lblZodiac: UILabel!
    @IBOutlet weak var lblGotra: UILabel!
    @IBOutlet weak var lblReligion: UILabel!
    @IBOutlet weak var lblCaste: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    var item: ProfileViewModelItem? {
        didSet {
            guard let item = item as? ViewModelCulturalInfoItem else {
                return
            }
            
            lblCaste?.text = item.caste
            lblReligion?.text = item.religion
            lblGotra?.text = item.gotra
            lblZodiac?.text = item.zodiac
            lblLanguages?.text = item.languages
        }
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
